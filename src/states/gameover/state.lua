local State = {}
State.__index = State

function State.init()
  return setmetatable({}, State)
end

function State:load()
end

function State:update(dt)
end

function State:draw()
end

return State