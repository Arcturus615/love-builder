local path = 'src/states/'

local states = {
  current = false,
  menu = require(path..'menu/state').init(),
  pause = require(path..'pause/state').init(),
  game = require(path..'game/state').init(),
  gameover = require(path..'gameover/state').init()
}

local function load(self, state)
  self.states.current = self.states[state]
  self.states.current:load()
  print('Loading: '..state)
end

local function reload(self, state)
  self.states[state] = nil
  self.states[state] = require(path..state..'/state').init()
  self:load(state)
end

local function toggleDebug(self)
  if self.debugmode then self.debugmode = false return end
  self.debugmode = true
end

local function update(self, dt)
  self.states.current:update(dt, self)
end

local function draw(self)
  self.states.current:draw(self)
end

return {
  load=load,
  update=update,
  draw=draw,
  reload=reload,
  toggleDebug=toggleDebug,

  states=states,
  debugmode=false
  }