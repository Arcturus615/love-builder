LIBS='src/common/'

require('src/utils')
function love.load()
	math.initRandom()
	gamestate = require('src/gamestate')
	gamestate:load('game')
end

function love.update(dt)
	gamestate:update(dt)
end

function love.draw()
	gamestate:draw()
end
