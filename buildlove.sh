#!/bin/bash
# Love builder

NAME="invaders"
BUILD="$(pwd)/bin"
FILES='main.lua|conf.lua|src|assets'
IGNORED='love.ico|game.ico|love.exe|lovec.exe|readme.txt|changes.txt'
LOVEVERSION="love-0.10.2"
TAG=""
# LIN32BUILD="$NAME-lin32"
# LIN64BUILD="$NAME-lin64"
WIN32BUILD="$NAME-win32.exe"
WIN64BUILD="$NAME-win64.exe"

match () {
  awk -v value="$1" -v pattern="$2" '
      BEGIN {
          if (value ~ pattern) {
              exit 0
          } else {
              exit 1
          }
      }'
}

processFiles () {
    for filename in *; do
        if match "$filename" "$FILES"; then
            zip -9 -r "$BUILD/${NAME}.love" "./$filename"
        fi
    done
}

generateExes () {
    cat "./love/$LOVEVERSION-win32/love.exe" "$BUILD/${NAME}.love" > "$BUILD/win32/$WIN32BUILD"
    cat "./love/$LOVEVERSION-win64/love.exe" "$BUILD/${NAME}.love" > "$BUILD/win64/$WIN64BUILD"
}

populateLibs () {
    local olddir
    olddir=$(pwd)
    cd "./love/$LOVEVERSION-win32/" || exit 1
    for filename in *; do
        if match "$filename" "$IGNORED"; then
            continue
        fi
        cp -u "$filename" "$BUILD/win32/"
    done

    cd "$olddir" || exit 1
    cd "./love/$LOVEVERSION-win64/" || exit 1
    for filename in *; do
        if match "$filename" "$IGNORED"; then
            continue
        fi
        echo "Copying: $filename"
        cp -u "$filename" "$BUILD/win64/"
    done
}

generateZips () {
    zip -9 -r "$BUILD/${NAME}-win32${TAG}.zip" "$BUILD/win32/"
    zip -9 -r "$BUILD/${NAME}-win64${TAG}.zip" "$BUILD/win64/"
}

if [ -a "$BUILD/${NAME}.love" ]; then rm "$BUILD/${NAME}.love"; fi
if [ -a "$BUILD/${NAME}-win32.zip" ]; then rm "$BUILD/${NAME}-win32${TAG}.zip"; fi
if [ -a "$BUILD/${NAME}-win64.zip" ]; then rm "$BUILD/${NAME}-win64${TAG}.zip"; fi

mkdir -p "$BUILD/lin32"
mkdir -p "$BUILD/lin64"
mkdir -p "$BUILD/win32"
mkdir -p "$BUILD/win64"

processFiles
generateExes
populateLibs
generateZips


exit 0
